# Azure Quota Exporter

[![Build Status](https://drone.0x01.host/api/badges/root/theneedyguy-azure-quota-exporter/status.svg?ref=master)](https://drone.0x01.host/root/theneedyguy-azure-quota-exporter)

This tool exports Azure Subscription limits and usages as easily digestable [Prometheus](https://prometheus.io/) metrics. Prometheus is a cloud-native open source monitoring solution. 

---

## Metrics and Labels

The exporter exposes 2 metrics to Prometheus.

```text
az_resource_quota_limit
az_resource_quota_usage
```

Labels are used to filter the desired values to monitor. The following labels are set for both metrics:

```text
subscription_id
subscription_name
resource_type
resource_category
```

### Example 

If you want to find all Azure Network resource usages for a certain subscription you could use this query in Prometheus:

```text
az_resource_quota_usage{resource_category="Network", subscription_name="Example Subscription"}
```
---
## Configuration

### Exporter

The exporter need to be configured using environment variables. There is no config file. You will need to create an [Azure Service Principal](https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-create-service-principal-portal) and grant it permissions over every subscription you want to get the resource quotas from. The Azure Builtin role "Contributor" should to the job. You could also use a Builtin role with less permissions but you will have to figure out which one is able to read subscription quotas.

All the following variables need to be set. There are no defaults!

- __CLIENT_ID__ (The ID of your Service Principal)
- __SECRET__ (The secret of the Service Principal)
- __TENANT__ (Your Azure tenant ID)
- __LISTEN_PORT__ (The port you want the metrics server to be exposed to)
- __SUBSCRIPTION_EXCLUSIONS__ (A comma separated list of **subscription IDs**. DO NOT USE SUBSCRIPTION NAMES!)
- __LOCATION__ (Location of your resources i.e. 'westeurope')

### Prometheus Config

**IMPORTANT:** If you're running both azure-quota-exporter and Prometheus in a Docker Container, set the name of the target as the name of the service you defined. Also set the port to the one you defined in the environment variable 'LISTEN_PORT'.

Set the following job in your Prometheus config file:

```yaml
scrape_configs:
  - job_name: 'azure-quota-exporter'
    scrape_interval: 60s
    static_configs:
      - targets: ['azure-quota-exporter:8000'] 
```

---

## Running the exporter

The exporter is designed to be run inside a Docker Container but could also be run outside of one (not recommended). 

Use the following command (substitute the placeholder values) to run the azure-quota-exporter:

```text
docker run -d -p "8000:8000" \
--env CLIENT_ID=yourid \
--env SECRET=yoursecret \
--env TENANT=tenantid \
--env LISTEN_PORT=8000 \
--env LOCATION=westeurope \
--env SUBSCRIPTION_EXCLUSIONS=your,comma,separated,subscription,ids \
ckevi/azure-quota-exporter
```

If you want to get resources across multple locations or tenants, run multiple instances of the container with the right environment variables. Also configure your Prometheus scrape config accordingly.

---

## FAQ

__Who is this for?__

The exporter is mainly useful for people who are in charge of an Azure Subscription who want to track the resource usage in the subscription w/o having to manually check.

__How do I alert when a certain threshold is reached?__

The easiest way to alert with Prometheus is running Alertmanager and configuring alert rules in Prometheus. [The Grafana team has a great guide](https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/) on how to do that.

__Why use Prometheus instead of a simple Powershell script to keep track?__

I would agree that using a fully fledged monitoring solution might seem a bit much (and honestly, creating a PS script will do the trick just fine for this purpose) but if you want to graphically display the current resource usage over time, Prometheus is a great tool for that. 

Prometheus also makes it immensely easy to Alert the right people, if you have configured alerting rules and the Prometheus Alertmanager. 

Teams that are already running Prometheus on their infrastructure will find this quota exporter extremely easy to integrate into their existing environment.

__Can I trust this script not to send my data to 3rd parties?__

Have a look at the [code](app.py). The application uses the official Microsoft SDK. No data will be sent anywhere except to Microsoft. I promise that no data is sent to any 3rd parties. I am not interested in your data.
If in doubt, build your own container.

__How is this project licensed?__

The project is licensed under the GPLv2.
