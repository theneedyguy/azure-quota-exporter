#!/usr/bin/env python3

from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.network import NetworkManagementClient
from azure.mgmt.storage import StorageManagementClient
from azure.mgmt.subscription import SubscriptionClient
from prometheus_client import start_http_server, Gauge
import os
import time


'''
Get all config from the environment.
'''
client_id = os.getenv("CLIENT_ID")
secret = os.getenv("SECRET")
tenant = os.getenv("TENANT")
listen_port = os.getenv("LISTEN_PORT")
resource_location = os.getenv("LOCATION")
excluded_subscriptions = os.getenv("SUBSCRIPTION_EXCLUSIONS")

'''
Set credentials using the env vars.
'''
credentials = ServicePrincipalCredentials(
    client_id=client_id,
    secret=secret,
    tenant=tenant
)

'''
Define Prometheus metrics
'''
RESOURCE_QUOTA_LIMIT = Gauge('az_resource_quota_limit', 'Current resource limit for an Azure resource', ['subscription_id', 'subscription_name', 'resource_type', 'resource_category', 'resource_location'])
RESOURCE_QUOTA_USAGE = Gauge('az_resource_quota_usage', 'Current resource usage for an Azure resource', ['subscription_id', 'subscription_name', 'resource_type', 'resource_category', 'resource_location'])

'''
Function that separates the expected comma separated subscription IDs.
Returns a list of subscription ids.
'''
def create_exclusion_list(subscriptions: str):
    # Check if subscriptions has any value
    if len(subscriptions) > 0 or subscriptions != "" or subscriptions != None:
        # Remove all whitespace and split on commas
        # Subscription IDs have no whitespace but maybe a user has the great idea to ignore the readme and add whitespace to the string.
        subscription_list = subscriptions.replace(" ", "").split(",")
        return subscription_list
    # If the exclusions are not defined return an empty list
    else:
        return []

'''
Gets all subscription infos.
Exclusions can be passed to filter out unwanted subscriptions.
'''
def get_subscription_ids(exclusions: list):
    sub_client = SubscriptionClient(credentials)
    subs = sub_client.subscriptions.list()
    subscriptions = []
    # Iterate through all subscriptions
    for sub in subs:
        # If you have exclusdions we only add subscriptions that are not in the exclusion list
        if len(exclusions) > 0: 
            if sub.state == "Enabled"  \
            and sub.subscription_id not in exclusions:
                subscriptions.append({"subscriptionId": sub.subscription_id, "subscriptionName": sub.display_name})
        # If there are no exclusions, add all subscriptions there are
        else:
            if sub.state == "Enabled":
                subscriptions.append({"subscriptionId": sub.subscription_id, "subscriptionName": sub.display_name})
    return subscriptions

'''
Gets all limits and quotas for Compute Resources
'''
def process_azure_compute(subscriptions: list):
    for sub in subscriptions:
        client = ComputeManagementClient(credentials, sub["subscriptionId"])
        try:
            usages_client = client.usage.list(resource_location)
            for use in usages_client:
                RESOURCE_QUOTA_USAGE.labels(sub["subscriptionId"], sub["subscriptionName"], use.name.value, "Compute", resource_location).set(use.current_value)
                RESOURCE_QUOTA_LIMIT.labels(sub["subscriptionId"], sub["subscriptionName"], use.name.value, "Compute", resource_location).set(use.limit)
        except:
            pass

'''
Gets all limits and quotas for Network Resources
'''
def process_azure_network(subscriptions: list):
    for sub in subscriptions:
        client = NetworkManagementClient(credentials, sub["subscriptionId"])
        try:
            usages_network = client.usages.list(resource_location)
            for use in usages_network:
                RESOURCE_QUOTA_USAGE.labels(sub["subscriptionId"], sub["subscriptionName"], use.name.value, "Network", resource_location).set(use.current_value)
                RESOURCE_QUOTA_LIMIT.labels(sub["subscriptionId"], sub["subscriptionName"], use.name.value, "Network", resource_location).set(use.limit)
        except:
            pass

'''
Gets all limits and quotas for Storage Resources
'''
def process_azure_storage(subscriptions: list):
    for sub in subscriptions:
        client = StorageManagementClient(credentials, sub["subscriptionId"])
        try:
            usages_storage = client.usage.list_by_location(resource_location)
            for use in usages_storage:
                RESOURCE_QUOTA_USAGE.labels(sub["subscriptionId"], sub["subscriptionName"], use.name.value, "Storage", resource_location).set(use.current_value)
                RESOURCE_QUOTA_LIMIT.labels(sub["subscriptionId"], sub["subscriptionName"], use.name.value, "Storage", resource_location).set(use.limit)
        except:
            pass


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    print("Starting up server...")
    try:
        int(listen_port)
    except:
        print(f'Value for variable LISTEN_PORT is {listen_port} and could not be converted to an integer. Please fix this issue and restart the application.')
        print('Stopping server.')
        exit
    start_http_server(int(listen_port))
    print(f'Server sucessfully started. Listening on port {listen_port}')
    print(f'Found the following exclusions: {excluded_subscriptions}')
    exclusions = create_exclusion_list(excluded_subscriptions)
    while True:
        all_subscriptions = get_subscription_ids(exclusions)
        process_azure_compute(all_subscriptions)
        process_azure_network(all_subscriptions)
        process_azure_storage(all_subscriptions)
        time.sleep(3600)
